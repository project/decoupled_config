<?php

namespace Drupal\decoupled_config;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class DecoupledConfig.
 *
 * @package Drupal\decoupled_config
 */
class DecoupledConfig implements DecoupledConfigInterface {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * @var \Drupal\decoupled_config\DecoupledConfigYamlDiscovery
   */
  private $decoupledConfigYamlDiscovery;

  /**
   * @var array
   */
  private $config = [];

  /**
   * @var array
   */
  private $configGrupedByModule = [];

  /**
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
    $this->scan();
    $this->process();
  }

  /**
   * Scan all decoupled config yaml files.
   */
  private function scan(): void {
    $this->decoupledConfigYamlDiscovery = new DecoupledConfigYamlDiscovery($this->moduleHandler->getModuleDirectories());
    $this->configGrupedByModule = $this->decoupledConfigYamlDiscovery->findAll();
  }

  /**
   * Processs scaned config.
   */
  private function process(): void {
    foreach ($this->configGrupedByModule as $module => $config_files) {
      foreach ($config_files as $config_name => $value) {
        if (empty($this->config[$config_name])) {
          $this->config[$config_name] = [];
        }
        $this->config[$config_name] = NestedArray::mergeDeep($this->config[$config_name], $value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): array {
    return !empty($this->config)
      ? $this->config
      : [];
  }

}
