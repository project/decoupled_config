<?php

namespace Drupal\decoupled_config\Helper;

use Drupal\Component\Utility\NestedArray;

/**
 * Class DecoupledConfigArrayHelper.
 *
 * @package Drupal\decoupled_config\Helper
 */
class DecoupledConfigArrayHelper {

  /**
   * @param array $array
   * @param array $prefixes
   * @param array $suffixes
   * @param string $glue
   *
   * @return array
   */
  public static function wrapItems(array $array, array $prefixes = [], array $suffixes = [], $glue = ':'): array {
    $wrap = [
      'prefixes' => $prefixes,
      'suffixes' => $suffixes,
      'glue' => $glue,
    ];
    array_walk($array, function (&$item, $key, $wrap) {
      $pieces = array_merge($wrap['prefixes'], [$item], $wrap['suffixes']);
      $item = implode($wrap['glue'], $pieces);
    }, $wrap);
    return $array;
  }

  /**
   * Get all array keys for each array value.
   *
   * @param array $array
   * @param array $array_path
   * @param array $result
   *
   * @return array
   */
  public static function getMultipleArrayPath(array $array, array &$array_path = [], array &$result = []): array {
    $result = [];
    foreach ($array as $key => $value) {
      $array_path[] = $key;
      if (is_array($value)) {
        static::getMultipleArrayPath($value, $array_path, $result);
      }
      else {
        $result[] = $array_path;
        array_pop($array_path);
      }
    }
    return $result;
  }

  /**
   * Remove array value through an array path.
   *
   * @param array $array
   * @param array $list_array_path
   * @param $value
   */
  public static function unsetListItemValue(array &$array, array $list_array_path, $value): void {
    $items = NestedArray::getValue($array, $list_array_path);

    foreach ($items as $key => $item) {
      if ($item === $value) {
        $item_array_path = array_merge($list_array_path, [$key]);
        NestedArray::unsetValue($array, $item_array_path);
        break;
      }
    }
  }

}
