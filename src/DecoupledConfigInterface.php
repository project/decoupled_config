<?php

namespace Drupal\decoupled_config;

/**
 * Interface DecoupledConfigInterface.
 *
 * @package Drupal\decoupled_config
 */
interface DecoupledConfigInterface {

  /**
   * @return array
   */
  public function getConfig(): array;

}
