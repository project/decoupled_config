<?php

namespace Drupal\decoupled_config\Plugin\ConfigFilter;

use Drupal\Component\Utility\NestedArray;
use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\decoupled_config\DecoupledConfigInterface;
use Drupal\decoupled_config\Helper\DecoupledConfigArrayHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config filter to add decoupled configurations based on yaml definitions.
 *
 * @ConfigFilter(
 *   id = "decoupled_config_filter",
 *   label = "Decoupled Config Filter",
 *   weight = 5
 * )
 */
class DecoupledConfigFilter extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\decoupled_config\DecoupledConfigInterface
   */
  private $decoupledConfig;

  /**
   * DecoupledConfigFilter constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\decoupled_config\DecoupledConfigInterface $decoupled_config
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DecoupledConfigInterface $decoupled_config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->decoupledConfig = $decoupled_config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('decoupled_config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function filterRead($name, $data) {
    if (!$this->source->getCollectionName()) {
      $config = $this->decoupledConfig->getConfig();
      if (!empty($config[$name])) {
        $data = NestedArray::mergeDeep($data, $config[$name]);
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function filterReadMultiple(array $names, array $data) {
    if (!$this->source->getCollectionName()) {
      $config = $this->decoupledConfig->getConfig();
      foreach ($config as $config_name => $values) {
        if (in_array($config_name, $names)) {
          $data[$config_name] = $this->filterRead($config_name, $data[$config_name]);
        }
      }
    }

    return $data;
  }

  /**
   * Method that override config yaml files.
   *
   * Exclude from export values imported previously by decoupled config.
   *
   * @param $name
   * @param array $data
   *
   * @return array
   */
  public function filterWrite($name, array $data) {
    if (!$this->source->getCollectionName()) {
      $config = $this->decoupledConfig->getConfig();
      if (!empty($config[$name])) {
        $multiple_array_path = DecoupledConfigArrayHelper::getMultipleArrayPath($config[$name]);

        foreach ($multiple_array_path as $array_path) {
          $last_value = end($array_path);

          // If last array path value is integer we consider like a list.
          if (is_int($last_value)) {
            $list_array_path = $array_path;
            array_pop($list_array_path);
            $value = NestedArray::getValue($config[$name], $array_path);
            DecoupledConfigArrayHelper::unsetListItemValue($data, $list_array_path, $value);
          }
          else {
            NestedArray::unsetValue($data, $array_path);
          }
        }
      }
    }
    return $data;
  }

}
