This module allow you decoupled configuration files split config items in
separated config files located in modules.

HOW TO USE:
Create decoupled_config directory in your module and put your config files
there.

Those config items will be merged in the import process with your
config/sync configurations to get the active configuration.

EXAMPLE:
config/sync/honeypot.settings.yml:
----------------------------------
log: false
expire: 300
form_settings:
 user_register_form: false
 user_pass: false
 comment_comment_form: false

my_module/decoupled_config/honeypot.settings.yml:
-----------------------------------------------
form_settings:
 node_course_form: false

active config:
--------------
log: false
expire: 300
form_settings:
 user_register_form: false
 user_pass: false
 comment_comment_form: false
 node_course_form: false
